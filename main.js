let table;
let firstName;
let lastName;
let age;
let btn;
let noDataRow;

const url = 'http://92.62.139.201:8080';

window.addEventListener('load', function(event) {
    table = document.getElementById('table');
    firstName = document.getElementById('firstName');
    lastName = document.getElementById('lastName');
    age = document.getElementById('age');
    btn = document.getElementById('btn');
    noDataRow = document.getElementById('no-data');
});

function fetchData(endpoint, callbackFn) {
    let http = new XMLHttpRequest();

    http.onreadystatechange = function(event) {
        if (this.readyState == 4 && this.status == 200) {
            callbackFn(JSON.parse(this.response));
        } else {
            // TODO error
        }
    };

    http.open("GET", url + endpoint);
    http.send();
}

// addData()
function storeData(endpoint, data, callbackFn) {
    let http = new XMLHttpRequest();
    
    http.onreadystatechange = function(event) {
        if (this.readyState == 4 && this.status == 200) {
            callbackFn(JSON.parse(this.response));
        } else {
            // TODO error
        }
    };

    http.open("POST", url + endpoint);
    http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    http.send(JSON.stringify(data));
}

function showUsers(data) {    
    if (data.length === 0) {
        noDataRow.style.display = 'table-row';
    } else {
        noDataRow.style.display = 'none';
    }

    resetTable();

    data.forEach(function(user) {
        appendTable(user.firstName, user.lastName, user.age);
    });
}

function validateInput(firstName, lastName, age) {
    return firstName && firstName.length >= 3 && lastName && lastName.length >= 3 && age >= 0;
}

function appendTable(firstName, lastName, age) {
    const row = document.createElement("tr");
    const td1 = document.createElement("td");
    const td2 = document.createElement("td");
    const td3 = document.createElement("td");

    td1.innerText = firstName;
    td2.innerText = lastName;
    td3.innerText = age;

    row.className = 'user-row';
    row.append(td1);
    row.append(td2);
    row.append(td3);

    table.append(row);
}

// optional example
function removeRows(rows) {
    if (rows.length > 0) {
        rows[rows.length - 1].remove();
        removeRows(rows);
    }
}

function resetTable() {
    const rows = document.getElementsByClassName('user-row');

    while (rows.length > 0) {
        rows[rows.length - 1].remove();
    }
}

function addEntry() {
    if (validateInput(firstName.value, lastName.value, age.value)) {
        storeData('/users', {
            firstName: firstName.value,
            lastName: lastName.value,
            age: age.value,
        }, function() {
            fetchData('/users', showUsers);
        });
    } else {
        alert('Invalid input!');      
    }
}

fetchData('/users', showUsers);
